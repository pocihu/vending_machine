var gulp = require('gulp'),
browserify = require('browserify'),
babelify = require('babelify'),
source = require('vinyl-source-stream'),
less = require('gulp-less'),
phpunit = require('gulp-phpunit'),
livereload = require('gulp-livereload');

var defaultTask = function(done) {
    livereload.listen();

    gulp.watch(['resources/assets/less/**/*.less'], css_compile);
    console.log('LESS watcher added');

    gulp.watch(['resources/assets/js/**/*.jsx'], js_compile);
    console.log('JS watcher added');

    gulp.watch(['app/**/*.php'], php_test);
    console.log('PHP watcher added');

    //Just 4 some views
    gulp.watch(['resources/views/**/*.php'], reload);

    return done();
}

var css_compile = function() {

    console.log('running css compile');

    return gulp.src(['resources/assets/less/main.less'])
    .pipe(less({
        paths: [
            '.',
            './node_modules/bootstrap-less'
        ]
    }))
    .on('error', errorHandler)
    .pipe(gulp.dest('public/css'))
    .pipe(livereload());
}

var js_compile = function(){

    console.log('running js compile');

    return browserify({
        entries: 'resources/assets/js/main.jsx',
        extensions: ['.js', '.jsx'],
        debug: true
    })
    .transform(babelify, {
        presets: ['env', 'react', 'stage-0']
    })
    .bundle()
    .pipe(source('main.js'))
    .pipe(gulp.dest('public/js'))
    .on('error', errorHandler)
    .pipe(livereload());

    // return gulp.src('resources/assets/js/main.js')
    // .on('error', errorHandler)
    // .pipe(gulp.dest('public/js'))
    // .pipe(livereload());
}

var php_test = function(){

    console.log('running php tests');

    var options = {debug: false};

    return gulp.src('phpunit.xml')
    .pipe(phpunit('./vendor/bin/phpunit', options))
    .on('error', errorHandler)
    .pipe(livereload());
};

var reload = function(done){
    livereload.reload();

    return done();
};

var errorHandler = function() {

    var arguments = Array.prototype.slice.call(arguments);

    console.log('Error occured: ');
    console.log(arguments);
    this.emit('end');
};

exports.default = gulp.series(css_compile, js_compile, defaultTask);
