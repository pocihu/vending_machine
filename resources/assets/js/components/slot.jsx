import React from 'react';

import Product from './product';

export default class Slot extends React.Component {

    getProducts = () => {

        var products = [];

        for (var i = 0; i < this.props.stock.quantity; i++)
        {
            var key = i,
            productClass = '',
            click = () => {};

            if (this.props.selling)
            {

                if (i)
                {
                    key = i - 1;
                } else {
                    productClass = ' selling';
                    click = this.props.click;
                }
            }

            products.push(
                <Product key={key} pkey={key} product={this.props.stock.product} class={productClass} click={click} />
            );
        }

        return products;
    }

    render = () => {
        return (
            <div className="slot">
                {this.getProducts()}
                <div className="code">
                    <span>
                        {this.props.slot_identifier}
                    </span>
                </div>
            </div>
        );
    }
}
