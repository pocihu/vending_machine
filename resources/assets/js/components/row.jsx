import React from 'react';

import Slot from './slot';

export default class Row extends React.Component {

    getSlot = (slot, i) => {

        var selling = this.props.state.change && this.props.state.selected == slot.slot_identifier;

        return <Slot key={i} {...slot} selling={selling} selected={this.props.state.selected} click={this.props.click} />;
    }

    getSlots = () => {
        return this.props.slots.map(this.getSlot);
    }

    render = () => {
        return (
            <div className={'row row-' + this.props.skey}>
                {this.getSlots()}
            </div>
        );
    }
}
