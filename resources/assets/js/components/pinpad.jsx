import React from 'react';

export default class Pinpad extends React.Component {

    handleClick = (e) => {

        var value = e.currentTarget.dataset.value;

        if (value == 'Ok')
        {
            //submit
            this.props.submit();
            return;
        }

        if (value == 'C')
        {
            //clear selected
            this.props.handle({
                selected: '',
                showing: 'inserted'
            });
            return;
        }

        //combine otherwise
        this.props.handle({
            selected: this.props.selected + value,
            showing: 'selected',
        });
    }

    getButtons = () => {

        return [1, 2, 3, 4, 5, 6, 7, 8, 9, 'C', 0, 'Ok'].map((button) => {
            return (
                <a key={button} className="pin-btn" data-value={button} onClick={this.handleClick}>
                    {button}
                </a>
            );
        });
    }

    render = () => {
        return (
            <div id="pinpad">
                {this.getButtons()}
            </div>
        );
    }
}
