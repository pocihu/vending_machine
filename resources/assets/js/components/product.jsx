import React from 'react';

export default class Product extends React.Component {

    render = () => {
        return (
            <div key={this.props.pkey} className={'product product-' + this.props.pkey + this.props.class} onClick={this.props.click} >
                <img src={'/assets/product-' + this.props.product.name.toLowerCase() + '.png'} />
            </div>
        );
    }
}
