import React from 'react';

export default class Coin extends React.Component {

    render = () => {
        return (
            <a className="coin" data-value={this.props.value} onClick={this.props.click}>
                <img src={'/assets/coin-' + this.props.value + '.png'} />
            </a>
        );
    }
}
