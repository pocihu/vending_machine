import React from 'react';

export default class Screen extends React.Component {

    render = () => {
        return (
            <div id="screen">
                {this.props.errored ? 'Error' : this.props.display}
            </div>
        );
    }
}
