import React from 'react';

import Screen from './screen';
import Pinpad from './pinpad';
import Coin from './coin';
import Row from './row';

export default class Machine extends React.Component {

    constructor (props) {
        super(props);

        this.setStock();

        this.state = {
            stock: [],
            inserted: 0,
            selected: '',
            selectedSlot: {},
            showing: 'inserted',
            change: 0,
        };
    }

    setStock = () => {
        $.get('/api/stock', (response) => {
            if (response.success)
            {
                this.setState({
                    stock: response.slots
                })
            } else {
                alert('Stock error!');
            }
        })
    }

    setState = (newState) => {

        //on any interaction, error msg shall disappear
        if (!newState.errored)
        {
            newState.errored = false;
        }

        return super.setState(newState);
    }

    getItems = () => {
        var rows = [];

        for (var i = 0; i < 5; i++)
        {
            rows.push(this.state.stock.slice((i * 5), (i * 5) + 5));
        }

        return (
            <div id="items">
                {rows.map((slots, j) => {
                    return  <Row key={j} skey={j} slots={slots} state={this.state} click={this.handleDone}/>;
                })}
            </div>
        );
    }

    getScreen = () => {
        return (
            <Screen errored={this.state.errored} display={this.state[this.state.showing]}/>
        );
    }

    getClear = () => {
        return <div id="clear" onClick={this.handleClear} />;
    }

    getPinpad = () => {

        return <Pinpad selected={this.state.selected} handle={this.handlePinpad} submit={this.handleSubmit} />;
    }

    getCoins = () => {
        return (
            <div id="coins-to-use" className="coin-container">
                <Coin value={200} click={this.handleCoin} />
                <Coin value={100} click={this.handleCoin} />
                <Coin value={50} click={this.handleCoin} />
            </div>
        );
    }

    getChange = () => {

        var change = 0;

        return (
            <div id="change-received" onClick={() => {this.handleDone();}}>
                {this.state.change ? this.state.change : ''}
            </div>
        );
    }

    getReceivedCoins = () => {

        if (this.state.change)
        {
            var stack = [];

            var compileStack = (remaining, i = 0) => {

                var available = [200, 100, 50, 20, 10, 5];

                if (remaining  && available[i])
                {
                    if (remaining / available[i] >= 1)
                    {
                        remaining = remaining - available[i];
                        stack.push(available[i]);

                        //check again for same coin
                        if (remaining / available[i] >= 1)
                        {
                            return compileStack(remaining, i);
                        }

                    }

                    return compileStack(remaining, i + 1);
                }
            }

            compileStack(this.state.change);

            return (
                <div id="coins-received">
                    {stack.map((coin, i) => {
                        return <Coin key={i} value={coin} click={this.handleDone} />;
                    })}
                </div>
            );
        }
    }

    handleCoin = (e) => {
        var value = parseInt(e.currentTarget.dataset.value);

        this.setState({
            inserted: this.state.inserted + value,
            selected: '',
            selectedSlot: {},
            showing: 'inserted',
            change: 0
        });
    }

    handleClear = () => {
        this.setState({
            inserted: 0,
            selected: 0,
            showing: 'inserted',
            change: this.state.inserted
        });

        setTimeout(() => {
            this.setState({change: 0});
        }, 3000);
    }

    handlePinpad = (values) => {

        this.setState(values);
    }

    handleSubmit = () => {

        //find stock
        var slot = this.state.stock.find((slot) => {
            return slot.slot_identifier == this.state.selected;
        });

        if (!slot || (slot.stock.product.price > this.state.inserted))
        {
            //err
            this.setState({
                selected: '',
                selectedSlot: {},
                errored: true
            });
            return;
        }

        //@TODO
        if (!slot.stock.quantity)
        {
            //Err
            this.setState({
                selected: '',
                selectedSlot: {},
                errored: true
            });

            return;
        }

        // We have the slot, we have quantity,
        // submit to server
        $.post('/api/stock/sold', {
            slot_id: slot.id
        });

        //animate
        this.setState({
            inserted: 0,
            selectedSlot: slot,
            change: this.state.inserted - slot.stock.product.price,
            showing: 'inserted'
        });

        this.timer = setTimeout(() => {
            this.handleStock();
        }, 3000);
    }

    handleStock = () => {

        //let's show the changes
        var newStock = this.state.stock.map((slot) => {

            if (this.state.selectedSlot.slot_identifier == slot.slot_identifier)
            {
                slot.stock.quantity--;
            }

            return slot;
        });

        this.setState({
            stock: newStock,
            selected: '',
            selectedSlot: {},
            change: 0,
        })
    }

    handleDone = () => {
        //i took my goodies
        if (this.timer)
        {
            clearTimeout(this.timer);
        }

        this.handleStock();
    }

    render = () => {
        return (
            <div>
                <div id="machine">
                    {this.getItems()}
                    {this.getScreen()}
                    {this.getClear()}
                    {this.getPinpad()}
                    {this.getChange()}
                </div>
                <div id="coins">
                    {this.getCoins()}
                    {this.getReceivedCoins()}
                </div>
            </div>
        );
    }
}
