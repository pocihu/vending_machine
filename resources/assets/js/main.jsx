import React from 'react';
import ReactDOM from 'react-dom';

global.$ = global.jQuery = require('jquery');

import Machine from './components/machine';

ReactDOM.render(
        <Machine />,
        document.getElementById('machine-container')
);
