<!DOCTYPE html>
<html lang="hu" dir="ltr">
<head>
    <meta charset="utf-8">
    <title>Vendingo</title>
    <link rel="stylesheet" href="/css/main.css" />
</head>
<body>
    <div id="machine-container">
    </div>
    <script src="/js/main.js"></script>
</body>
</html>
