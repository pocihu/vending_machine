<?php

use Illuminate\Database\Seeder;
use App\Slot;

class SlotSeeder extends Seeder {

    /**
    * Run the database seeds.
    *
    * @return void
    */
    public function run()
    {
        DB::table('slots')->delete();

        // 25 slot van
        // Mindnek adunk szamot

        $slot_count = 0;

        while($slot_count < 25)
        {
            Slot::create([
                'machine_id' => 1,
                'slot_identifier' => ++$slot_count,
            ]);
        }
    }
}
