<?php

use Illuminate\Database\Seeder;
use App\Machine;

class StockSeeder extends Seeder {

    /**
    * Run the database seeds.
    *
    * @return void
    */
    public function run()
    {
        DB::table('stock')->delete();

        $slots = DB::table('slots')->get();
        $products = DB::table('products')->get();

        foreach ($slots as $slot)
        {
            $product = $products->random();

            DB::table('stock')->insert([
                'quantity' => 3,
                'slot_id' => $slot->id,
                'product_id' => $product->id,
            ]);
        }
    }
}
