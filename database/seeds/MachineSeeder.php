<?php

use Illuminate\Database\Seeder;
use App\Machine;

class MachineSeeder extends Seeder {

    /**
    * Run the database seeds.
    *
    * @return void
    */
    public function run()
    {
        DB::table('machines')->delete();

        Machine::create();
    }
}
