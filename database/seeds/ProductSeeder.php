<?php

use Illuminate\Database\Seeder;
use App\Product;

class ProductSeeder extends Seeder {

    /**
    * Run the database seeds.
    *
    * @return void
    */
    public function run()
    {
        DB::table('products')->delete();

        Product::create([
            'name' => 'Bomba',
            'price' => 315
        ]);

        Product::create([
            'name' => 'Mars',
            'price' => 190
        ]);

        Product::create([
            'name' => 'Snickers',
            'price' => 220
        ]);
    }
}
