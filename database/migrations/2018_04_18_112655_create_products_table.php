<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name', 45);
            $table->integer('price');
        });

        Schema::create('stock', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('quantity');
            $table->integer('slot_id')->unsigned();
            $table->integer('product_id')->unsigned();

            $table->foreign('slot_id')
            ->references('id')
            ->on('slots')
            ->onDelete('cascade');

            $table->foreign('product_id')
            ->references('id')
            ->on('products')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stock');
        Schema::dropIfExists('products');
    }
}
