<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class ControllerTest extends TestCase
{
    use DatabaseMigrations;

    public function setUp()
    {
        parent::setUp();

        $this->artisan('migrate:refresh');
        $this->artisan('db:seed');
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testController()
    {
        $this->get('/');

        $this->assertEquals(200, $this->response->status());

    }

    public function testApiStock()
    {
        $this->get('api/stock')
        ->seeJson([
            'success' => true
        ]);

        $this->assertEquals(200, $this->response->status());
    }

    public function testApiStockChange()
    {
        $this->call('POST', 'api/stock/sold', ['slot_id' => 1]);

        $this->assertEquals(200, $this->response->status());

        app('db')->table('stock')->where('slot_id', 1)->update(['quantity' => 0,]);

        $this->call('POST', 'api/stock/sold', ['slot_id' => 1]);

        $this->assertEquals(400, $this->response->status());

    }
}
