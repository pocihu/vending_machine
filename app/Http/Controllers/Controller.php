<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    public function index()
    {
        return view('index');
    }

    public function stock()
    {        
        $slots = \App\Machine::first()->slots;

        return response()->json(['success' => true, 'slots' => $slots]);
    }

    public function sold()
    {
        $slot_id = app('request')->get('slot_id');

        $slot = \App\Slot::findOrFail($slot_id);


        if (!$slot->stock->quantity)
        {
            return response()->json(['success' => false], 400);
        }

        $slot->stock->quantity--;
        $slot->stock->save();

        return response()->json(['success' => true]);
    }

}
