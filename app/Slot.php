<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slot extends Model
{
    public $timestamps = false;

    protected $with = ['stock'];
    // protected $appends = ['stock'];

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
        'machine_id', 'slot_identifier'
    ];

    public function machine()
    {
        return $this->belongsTo(Machine::class);
    }

    public function stock()
    {
        return $this->hasOne(Stock::class);
    }
}
