<?php

namespace App\Events;

class StockUpdating extends Event
{
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($model)
    {
        \App\History::create([
            'slot_id' => $model->slot_id,
            'product_id' => $model->product_id,
            'change' => $model->original['quantity'] - $model->quantity,
            'created_at' => \Carbon\Carbon::now()
        ]);
    }
}
