<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
    protected $table = 'stock';

    public $timestamps = false;

    protected $with = ['product'];

    protected $dispatchesEvents = [
        'updating' => Events\StockUpdating::class
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'quantity', 'slot_id', 'product_id',
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function slot()
    {
        return $this->belongsTo(Slot::class);
    }
}
